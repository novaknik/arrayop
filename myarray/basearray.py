from builtins import print
from typing import Tuple
from typing import List
from typing import Union
import itertools
import math

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """

    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """
        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0] * n
            elif dtype == float:
                self.__data = [0.0] * n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def data(self):
        return self.__data

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype
    def setdtype(self,type1):
        data1 = tuple(self)
        shape1 = self.shape
        dtype1 = float
        self=BaseArray(shape=shape1,data=data1,dtype=float)

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,) + ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n] * s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True

def _izpis(mat):
    if (len(mat.shape) == 1):
        for i in mat:
            print(i, end=" ", flush=True)
        print("")
    if (len(mat.shape) == 2):
        s = mat.shape[0]
        d = mat.shape[1]
        r = 0
        str1=""
        for p in mat:
            if r < (len(str(p))):
                r = len(str(p))
        for i in range(s):
            for j in range(d):
                for a in range(r - len(str(mat[i, j]))):
                    str1+=" "
                    print(" ", end="", flush=True)
                print(mat[i, j], end=" ", flush=True)
                str1+=str(mat[i, j])
                str1+=" "
            print("")
            str1+="\n"
        return str1

    if (len(mat.shape) == 3):
        s = mat.shape[0]
        d = mat.shape[1]
        v = mat.shape[2]
        r = 0
        str1=""
        for p in mat:
            if r < (len(str(p))):
                r = len(str(p))
        for k in range(v):
            for i in range(s):
                for j in range(d):
                        for a in range(r+1 - len(str(mat[i, j, k]))):
                            str1+=" "
                            print(" ", end="", flush=True)
                        print(mat[i, j, k], end=" ", flush=True)
                        str1+=str(mat[i, j, k])
                print("")
                str1+="\n"
            print("")
        return str1
def mergeSort(list):
    if len(list)>1:

        mid = len(list)//2
        lefthalf = list[:mid]
        righthalf = list[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                list[k]=lefthalf[i]
                i=i+1
            else:
                list[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < len(lefthalf):
            list[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < len(righthalf):
            list[k]=righthalf[j]
            j=j+1
            k=k+1

def _sortiraj1D(list):
    leng = list.shape[0]
    if leng > 1:
        mid = leng // 2
        lefthalf = list[:mid]
        righthalf = list[mid:]

        mergeSort(lefthalf)
        mergeSort(righthalf)

        i=0
        j=0
        k=0
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                list[k]=lefthalf[i]
                i=i+1
            else:
                list[k]=righthalf[j]
                j=j+1
            k=k+1

        while i < len(lefthalf):
            list[k]=lefthalf[i]
            i=i+1
            k=k+1

        while j < len(righthalf):
            list[k]=righthalf[j]
            j=j+1
            k=k+1

def _iskanje(mat,stevilo):
    if(len(mat.shape) == 1):
        ret=""
        for i in range(mat.shape[0]):
            if(mat[i] == stevilo):
                ret+=f"({i}),"
                print(f"({i}),",end=" ", flush=True)
        return ret
    if (len(mat.shape) == 2):
        s = mat.shape[0]
        d = mat.shape[1]
        ret=""
        for i in range(s):
            for j in range(d):
                if(mat[i,j] == stevilo):
                    ret+=f"({i},{j}),"
                    print(f"({i},{j}),",end="", flush=True)
        return ret
    if (len(mat.shape) == 3):
        s = mat.shape[0]
        d = mat.shape[1]
        v = mat.shape[2]
        ret=""
        for i in range(s):
            for j in range(d):
                for k in range(v):
                    if (mat[i, j,k] == stevilo):
                        ret+=f"({i},{j},{k}),"
                        print(f"({i},{j},{k}),", end="", flush=True)
        return ret

def sestej(mat, skalar):
    if isinstance(skalar, int) or isinstance(skalar, float):
        if (len(mat.shape) == 1):
            for i in range(mat.shape[0]):
                mat[i] += skalar

        if (len(mat.shape) == 2):
            s = mat.shape[0]
            d = mat.shape[1]
            for i in range(s):
                for j in range(d):
                    mat[i, j] = mat[i, j] + skalar
        if(len(mat.shape) == 3):
            s = mat.shape[0]
            d = mat.shape[1]
            v = mat.shape[2]
            for i in range(s):
                for j in range(d):
                    for k in range(v):
                        mat[i, j, k] = mat[i, j, k] + skalar
    else:
        if (len(mat.shape) == 1):
            if mat.shape[0] == skalar.shape[0]:
                for i in range(mat.shape[0]):
                    mat[i] += skalar[i]
        if (len(mat.shape) == 2):
            if mat.shape[0] == skalar.shape[0] and mat.shape[1] == skalar.shape[1]:
                for i in range(mat.shape[0]):
                    for j in range(mat.shape[1]):
                        mat[i,j] += skalar[i,j]

        if (len(mat.shape) == 3):
            if mat.shape[0] == skalar.shape[0] and mat.shape[1] == skalar.shape[1] and mat.shape[2] == skalar.shape[2]:
                for i in range(mat.shape[0]):
                    for j in range(mat.shape[1]):
                        for k in range(mat.shape[2]):
                            mat[i,j,k] += skalar[i,j,k]

def odsetvanje(mat, skalar):
    if isinstance(skalar, int) or isinstance(skalar, float):
        if (len(mat.shape) == 1):
            for i in range(mat.shape[0]):
                mat[i] -= skalar

        if (len(mat.shape) == 2):
            s = mat.shape[0]
            d = mat.shape[1]
            for i in range(s):
                for j in range(d):
                    mat[i, j] = mat[i, j] - skalar
        if(len(mat.shape) == 3):
            s = mat.shape[0]
            d = mat.shape[1]
            v = mat.shape[2]
            for i in range(s):
                for j in range(d):
                    for k in range(v):
                        mat[i, j, k] = mat[i, j, k] - skalar
    else:
        if (len(mat.shape) == 1):
            if mat.shape[0] == skalar.shape[0]:
                for i in range(mat.shape[0]):
                    mat[i] -= skalar[i]
        if (len(mat.shape) == 2):
            if mat.shape[0] == skalar.shape[0] and mat.shape[1] == skalar.shape[1]:
                for i in range(mat.shape[0]):
                    for j in range(mat.shape[1]):
                        mat[i,j] -= skalar[i,j]

        if (len(mat.shape) == 3):
            if mat.shape[0] == skalar.shape[0] and mat.shape[1] == skalar.shape[1] and mat.shape[2] == skalar.shape[2]:
                for i in range(mat.shape[0]):
                    for j in range(mat.shape[1]):
                        for k in range(mat.shape[2]):
                            mat[i,j,k] -= skalar[i,j,k]
def množenje(mat, skalar):
    if (len(mat.shape) == 1):
        for i in range(mat.shape[0]):
            mat[i] *= skalar

    if (len(mat.shape) == 2):
        s = mat.shape[0]
        d = mat.shape[1]
        for i in range(s):
            for j in range(d):
                mat[i, j] = mat[i, j] * skalar
    if(len(mat.shape) == 3):
        s = mat.shape[0]
        d = mat.shape[1]
        v = mat.shape[2]
        for i in range(s):
            for j in range(d):
                for k in range(v):
                    mat[i, j, k] = mat[i, j, k] * skalar
def deljenje(mat, skalar):
    if isinstance(skalar, int) or isinstance(skalar, float):
        if (len(mat.shape) == 1):
            for i in range(mat.shape[0]):
                mat[i] /= skalar

        if (len(mat.shape) == 2):
            s = mat.shape[0]
            d = mat.shape[1]
            for i in range(s):
                for j in range(d):
                    mat[i, j] = mat[i, j] / skalar
        if(len(mat.shape) == 3):
            s = mat.shape[0]
            d = mat.shape[1]
            v = mat.shape[2]
            for i in range(s):
                for j in range(d):
                    for k in range(v):
                        mat[i, j, k] = mat[i, j, k] / skalar
    else:
        if (len(mat.shape) == 1):
            if mat.shape[0] == skalar.shape[0]:
                for i in range(mat.shape[0]):
                    mat[i] /= skalar[i]
        if (len(mat.shape) == 2):
            if mat.shape[0] == skalar.shape[0] and mat.shape[1] == skalar.shape[1]:
                for i in range(mat.shape[0]):
                    for j in range(mat.shape[1]):
                        mat[i,j] /= skalar[i,j]

        if (len(mat.shape) == 3):
            if mat.shape[0] == skalar.shape[0] and mat.shape[1] == skalar.shape[1] and mat.shape[2] == skalar.shape[2]:
                for i in range(mat.shape[0]):
                    for j in range(mat.shape[1]):
                        for k in range(mat.shape[2]):
                            mat[i,j,k] /= skalar[i,j,k]
def log(mat,osnova):
    if (len(mat.shape) == 1):
        for i in range(mat.shape[0]):
            mat[i] = math.log(mat[i],osnova)

    if (len(mat.shape) == 2):
        s = mat.shape[0]
        d = mat.shape[1]
        for i in range(s):
            for j in range(d):
                mat[i, j] = math.log(mat[i, j],osnova)
    if(len(mat.shape) == 3):
        s = mat.shape[0]
        d = mat.shape[1]
        v = mat.shape[2]
        for i in range(s):
            for j in range(d):
                for k in range(v):
                    mat[i, j, k] = math.log(mat[i, j, k],osnova)

def pow(mat,osnova):
    if (len(mat.shape) == 1):
        for i in range(mat.shape[0]):
            mat[i] = math.pow(mat[i],osnova)

    if (len(mat.shape) == 2):
        s = mat.shape[0]
        d = mat.shape[1]
        for i in range(s):
            for j in range(d):
                mat[i, j] = math.pow(mat[i, j],osnova)
    if(len(mat.shape) == 3):
        s = mat.shape[0]
        d = mat.shape[1]
        v = mat.shape[2]
        for i in range(s):
            for j in range(d):
                for k in range(v):
                    mat[i, j, k] = math.pow(mat[i, j, k],osnova)

def mnozenje_matik(mat1,mat2):
    if(mat1.shape[1] == mat2.shape[0]):
        a = BaseArray((mat1.shape[0], mat2.shape[1]), dtype=float)
        for i in range(mat1.shape[0]):
            # iterate through columns of Y
            for j in range(mat2.shape[1]):
                # iterate through rows of Y
                for k in range(mat2.shape[0]):
                    a[i, j] += (mat1[i, k]*mat2[k, j])
        return a
    else:
        print("niso prave dimenzije")
        return -1

def sort_1D(matrika):
    polje = list(matrika)
    mergeSort(polje)
    for idx in range(len(matrika.data)):
        matrika[idx] = polje[idx]

def sort_2D(matrika,stevila):
    if(stevila == 0): # vrstica
        polje = list(matrika.data)
        matrikalist=list()

        kolkojihje = matrika.shape[1]
        kolkokrat = matrika.shape[0]
        for i in range(kolkokrat):
            matrikalist.append(polje[i*kolkojihje:(i+1)*kolkojihje])
        for i in range(len(matrikalist)):
            mergeSort(matrikalist[i])
        matrikalist = list(itertools.chain.from_iterable(matrikalist))
        for idx in range(len(matrika.data)):
            matrika.data[idx] = matrikalist[idx]

    if(stevila == 1): #stolpec
        polje = list(matrika.data)
        matrikalist = list()

        kolkojihje = matrika.shape[1]
        kolkokrat = matrika.shape[0]
        for i in range(kolkojihje):
            matrikalist.append(polje[i::kolkojihje])
        for i in range(len(matrikalist)):
            mergeSort(matrikalist[i])
        #matrikalist = list(itertools.chain.from_iterable(matrikalist))
        index=0
        #print(matrikalist)
        for i in range(kolkokrat):
            for j in range(kolkojihje):
                matrika.data[index] = matrikalist[j][i]
                index+=1
a1 = BaseArray((3,2),dtype=int, data=(1,2,3,4,5,6))
a2 = BaseArray((3,2),dtype=int, data=(1,2,3,4,5,6))
a3 = BaseArray((2,3),dtype=int, data=(1, 2, 3, 4 ,5 ,6))


mnozenje_matik(a1,a2)



#print(a1.dtype)
#sort_1D(a1)
#_izpis(a1)
#print("")
#sort_2D(a1,1)
#_izpis(a1)
#a21 = BaseArray((2,2), data=(1, 10, 3, 4))
#a22 = BaseArray((2,2), data=(1, 10, 3, 4))

#a31 = BaseArray((2,2,2), data=(1, 2, 3, 4, 5, 6, 7, 10))
#a32 = BaseArray((2,2,2), data=(1, 2, 3, 4, 5, 6, 7, 10))
#sestej(a31,a32)
#_izpis(a31)
#polje = a1.data
#a4 = BaseArray((2,2,2), data=tuple(polje))
#pow(a21,2)
#_izpis(a21)
#a23 = mnozenje_matik(a21,a22)
#_izpis(a23)